function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function appendToLog(output) {
    var text = $("#output").html();
    text = text + output;
    $("#output").html(text);
}

function clearLog() {
    $.ajax({
        type: "GET",
        url: "/clear",
        data: {},
        success: function(data) {
            console.log(data);
        }
    });
    $("#output").html("");
}

function disableCommands() {
    $("button", "#table_commands").prop("disabled",true);
    $("#loading-icon").show();
}

function enableCommands() {
    $("button", "#table_commands").prop("disabled",false);
    $("#loading-icon").hide();
}

function hideAllPopups() {
    $(".popup").each(function() {
        $(this).hide();
    });
}


function runReset(board, site, type) {
    disableCommands();
    // appendToLog("<b>Resetting TTC...</b><br>");
    $.ajax({
        method: "GET",
        url: "/" + board + "/fpga/" + site + "/reset/" + type,
        data: {},
        success: function(data) {
            // appendToLog(data);
            running = false;
            $.ajax({
                type: "GET",
                url: "/log",
                data: {},
                success: function(data) {
                    $("#output").html(data);
                }
            });
            enableCommands();
        }
    });
    addToLog();
}

function resetPopup(yesFn, noFn, cancelFn) {
    var popup = $("#reset_popup");
    popup.show();
    popup.find(".internal,.external,.cancel").unbind().click(function () {
        popup.hide();
    });
    popup.find(".internal").click(yesFn);
    popup.find(".external").click(noFn);
    popup.find(".cancel").click(cancelFn);
}

function reset(board, site) {
    hideAllPopups();
    resetPopup(function yes() {
        runReset(board, site, 'internal');
    }, function no() {
        runReset(board, site, 'external');
    }, function cancel() {
        //do nothing
    });
}


function genericPopup(div_id, yesFn, cancelFn) {
    var popup = $(div_id);
    popup.show();
    popup.find(".confirm,.cancel").unbind().click(function () {
        popup.hide();
    });
    popup.find(".confirm").click(yesFn);
    popup.find(".cancel").click(cancelFn);
}

function buffers(board, site) {
    hideAllPopups();
    genericPopup("#buffer_config", function yes() {
        disableCommands();
        var form = $("#buffer_form");
        var form_data = new FormData(form[0]);
        var url = form.attr('action');
        $.ajax({
            type: "POST",
            url: url,
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            success: function(data) {
                running = false;
                enableCommands();
                $.ajax({
                    type: "GET",
                    url: "/log",
                    data: {},
                    success: function(data) {
                        $("#output").html(data);
                        var output_div = document.getElementById("output");
                        output_div.scrollTop = output_div.scrollHeight;
                    }
                });
            }
        });
    }, function cancel() {
        //do nothing
    });
}


function mgts(board, site) {
    hideAllPopups();
    genericPopup("#mgts_config", function yes() {
        disableCommands();
        var form = $("#mgts_form");
        var form_data = new FormData(form[0]);
        var url = form.attr('action');
        $.ajax({
            type: "POST",
            url: url,
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            success: function(data) {
                running = false;
                enableCommands();
                $.ajax({
                    type: "GET",
                    url: "/log",
                    data: {},
                    success: function(data) {
                        $("#output").html(data);
                        var output_div = document.getElementById("output");
                        output_div.scrollTop = output_div.scrollHeight;
                    }
                });
            }
        });
    }, function cancel() {
        //do nothing
    });
}


function capture(board, site) {
    hideAllPopups();
    genericPopup("#capture_config", function yes() {
        disableCommands();
        var form = $("#capture_form");
        var form_data = new FormData(form[0]);
        var url = form.attr('action');
        $.ajax({
            type: "POST",
            url: url,
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            success: function(data) {
                running = false;
                enableCommands();
                $.ajax({
                    type: "GET",
                    url: "/log",
                    data: {},
                    success: function(data) {
                        $("#output").html(data);
                        var output_div = document.getElementById("output");
                        output_div.scrollTop = output_div.scrollHeight;
                    }
                });
                var date = new Date();
                var formattedDate = moment(date).format('YYYYMMDDhhmmss');
                location.href = "/" + board + "/fpga/" + site + "/capture/" + formattedDate + "_output.zip";
            }
        });
        addToLog();
    }, function cancel() {
        //do nothing
    });
}

function info(board, site) {
    disableCommands();
    // appendToLog("<b>Getting firmware information...</b><br>");
    $.ajax({
        method: "GET",
        url: "/" + board + "/fpga/" + site + "/info",
        data: {},
        success: function(data) {
            // appendToLog(data);
            enableCommands();
            running = false;
            $.ajax({
                type: "GET",
                url: "/log",
                data: {},
                success: function(data) {
                    $("#output").html(data);
                    var output_div = document.getElementById("output");
                    output_div.scrollTop = output_div.scrollHeight;
                }
            });
        }
    });
    addToLog();
}

function program(board, site) {
    hideAllPopups();
    genericPopup("#program_config", function yes() {
        disableCommands();
        // appendToLog("<b>Programming FPGA...</b><br>");
        var form = $("#program_form");
        var form_data = new FormData(form[0]);
        var url = form.attr('action');
        $.ajax({
            type: "POST",
            url: url,
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            success: function(data) {
                // appendToLog(data);
                enableCommands();
                running = false;
                $.ajax({
                    type: "GET",
                    url: "/log",
                    data: {},
                    success: function(data) {
                        $("#output").html(data);
                        var output_div = document.getElementById("output");
                        output_div.scrollTop = output_div.scrollHeight;
                    }
                });
                info(board, site);
            }
        });
        addToLog();
    }, function cancel() {
        //do nothing
    });
}


// ------------ jQuery Functions --------------

function manageMgtsRxTxOptions() {
    if ($("input[name=mode]:checked", "#mgts_form").val() != "configure") {
        $("input[name=rxtx]", "#mgts_form").each(function(i) {
            this.checked = false;
        });
        $("input[name=rxtx]", "#mgts_form").attr('disabled', true);
        $("#mgts_rxtx").css("background-color", "#eee");
        $("#mgts_rxtx").css("color", "#ddd");
    } else {
        $("input[name=rxtx]", "#mgts_form").attr('disabled', false);
        $("#mgts_rxtx").css("background-color", "#ffffff");
        $("#mgts_rxtx").css("color", "#000");
    }
}

$(document).ready(function(){
    $('#mgts_form input[name=mode]').on("change", function() {
        manageMgtsRxTxOptions();
    });
    manageMgtsRxTxOptions();
    $("#loading-icon").hide();
});


var running = false;

async function addToLog() {
    running = true;
    while (running) {
        $.ajax({
            type: "GET",
            url: "/log",
            data: {},
            success: function(data) {
                $("#output").html(data);
                var output_div = document.getElementById("output");
                output_div.scrollTop = output_div.scrollHeight;
            }
        });
        await sleep(500);
    }
}
