#!/usr/bin/env bash

export FLASK_APP=app.py
export FLASK_ENV=development
export FLASK_RUN_PORT=5123
export LC_ALL=en_GB.utf8
export LANG=en_GB.utf8
python3 -m flask run --host=0.0.0.0
