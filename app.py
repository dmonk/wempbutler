from flask import Flask
from flask import render_template
from flask import request
from flask import send_file
from werkzeug.exceptions import BadRequestKeyError

import os
import json

from utils import runCommand

app = Flask(__name__)

config = json.load(open("config.json"))


@app.route('/')
def index():
    """ Index page.
    """
    print(config["boards"])
    context = {
        'boards': config["boards"]
    }
    return render_template('index.html', **context)


@app.route('/<board>/')
def boards(board: str):
    """Board selector page.
    """
    context = {
        'board': board,
        'fpgas': config["boards"][board]["fpgas"]
    }
    return render_template('board.html', **context)


@app.route('/<board>/fpga/<site>')
def fpga(board: str, site: str):
    """ Main page for running Empbutler commands

    :param site:

    """
    context = {
        'site': site,
        'board': board,
        'commands': config["empbutler"]["commands"],
        'buffers': config["empbutler"]["buffers"],
        'mgts': config["empbutler"]["mgts"]
    }
    try:
        os.remove("log.txt")
    except FileNotFoundError:
        pass
    return render_template('fpga.html', **context)


@app.route('/<board>/fpga/<site>/reset/<type>')
def reset(board: str, site: str, type: str):
    """Callback request for resetting the FPGA.

    :param site: site of FPGA to run command on.
    :param type: which reset to execute, must be 'internal' or 'external'

    """
    command = config["empbutler"]["interface-command"] % (
        config["boards"][board]["port"],
        config["empbutler"]["binary-path"],
        config["empbutler"]["connections-file"]
    )
    command += "%s reset %s >> log.txt" % (site, type)
    runCommand("echo '<b>--- Resetting TTC ---</b>' >> log.txt")
    output = runCommand(command)
    runCommand("echo '<b>-----------------------------</b>\n' >> log.txt")
    context = {
        'output': [command] + output.split('\n'),
    }
    return render_template('output.html', **context)


@app.route('/<board>/fpga/<site>/buffers', methods=['GET', 'POST'])
def buffers(board: str, site: str):
    """ Callback request for configuring the buffers on the FPGA.

    :param site: site of FPGA to run command on.

    """
    if request.method == 'POST':
        form = request.form
        try:
            if form['upload_bool'] == 'on':
                f = request.files['file']
                f.save('uploads/uploaded_file.txt')
                form = request.form
                runCommand(
                    "scp -P 222 uploads/uploaded_file.txt "
                    + "cmx@pcuptracker001:/home/cmx/tmpfiles/pattern.txt >> log.txt"
                )
        except BadRequestKeyError:
            pass
    print(form)
    command = config["empbutler"]["interface-command"] % (
        config["boards"][board]["port"],
        config["empbutler"]["binary-path"],
        config["empbutler"]["connections-file"]
    )
    command += "%s buffers %s %s --chans %s " % (
        site, form['rxtx'], form['mode'], form['channels']
    )
    try:
        if form['upload_bool'] == 'on':
            command += "--inject file://tmpfiles/pattern.txt"
    except BadRequestKeyError:
        pass
    command += " >> log.txt"
    runCommand("echo '<b>--- Buffer configuration ---</b>' >> log.txt")
    output = runCommand(command)
    runCommand("echo '<b>-----------------------------</b>\n' >> log.txt")
    context = {
        'output': [command] + output.split('\n'),
    }
    try:
        if form['upload_bool'] == 'on':
            os.remove('uploads/uploaded_file.txt')
    except BadRequestKeyError:
        pass
    runCommand("ssh -p %d cmx@pcuptracker001 rm -rf /home/cmx/tmpfiles/pattern.txt" % (
        config["boards"][board]["port"]
    ))
    return render_template('output.html', **context)


@app.route('/<board>/fpga/<site>/mgts', methods=['GET', 'POST'])
def mgts(board: str, site: str):
    """Callback request for configuring the MGTs.

    :param site: site of FPGA to run command on.

    """
    if request.method == 'POST':
        form = request.form
    command = config["empbutler"]["interface-command"] % (
        config["boards"][board]["port"],
        config["empbutler"]["binary-path"],
        config["empbutler"]["connections-file"]
    )
    command += "%s mgts %s --chans %s" % (
        site, form['mode'], form['channels']
    )
    try:
        command += " %s >> log.txt" % form['rxtx']
    except BadRequestKeyError:
        pass
    runCommand("echo '<b>--- MGTs Setup ---</b>' >> log.txt")
    output = runCommand(command)
    runCommand("echo '<b>-----------------------------</b>\n' >> log.txt")
    print(output)
    context = {
        'output': [command] + output.split('\n'),
    }
    return render_template('output.html', **context)


@app.route('/<board>/fpga/<site>/capture', methods=['GET', 'POST'])
def capture(board: str, site: str):
    """Callback request for running the data capture.

    :param site: site of FPGA to run command on.

    """
    if request.method == 'POST':
        form = request.form
    command = config["empbutler"]["interface-command"] % (
        config["boards"][board]["port"],
        config["empbutler"]["binary-path"],
        config["empbutler"]["connections-file"]
    )
    command += "%s capture" % (site)
    try:
        if form['rx'] == 'on':
            command += " --rx %s" % form['rx_channels']
    except BadRequestKeyError:
        pass
    try:
        if form['tx'] == 'on':
            command += " --tx %s" % form['tx_channels']
    except BadRequestKeyError:
        pass
    command += " -o /home/cmx/tmpfiles/data/ >> log.txt"
    runCommand("echo '<b>--- Capturing data ---</b>' >> log.txt")
    output = runCommand(command)
    runCommand("echo '<b>-----------------------------</b>\n' >> log.txt")
    context = {
        'output': [command] + output.split('\n'),
    }
    return render_template('output.html', **context)


@app.route('/<board>/fpga/<site>/capture/<filename>', methods=['GET', 'POST'])
def captureDownload(board: str, site: str, filename: str):
    """Callback request for downloading the captured data.

    :param site: site of FPGA to run command on.

    """
    runCommand("rm -rf outputs/*")
    print(runCommand(
        "scp -r -P %d cmx@pcuptracker001:/home/cmx/tmpfiles/data outputs/" % (
            config["boards"][board]["port"])))
    runCommand("ssh -p %d cmx@pcuptracker001 rm -rf /home/cmx/tmpfiles/data" % (
        config["boards"][board]["port"]
    ))
    print(runCommand("zip -r outputs/" + filename + " outputs/data"))
    return send_file("outputs/" + filename, as_attachment=True)


@app.route('/<board>/fpga/<site>/info')
def info(board: str, site: str):
    """Callback request for requesting firmware information.

    :param site: site of FPGA to run command on.

    """
    command = config["empbutler"]["interface-command"] % (
        config["boards"][board]["port"],
        config["empbutler"]["binary-path"],
        config["empbutler"]["connections-file"]
    )
    command += "%s info >> log.txt" % (site)
    runCommand("echo '<b>--- Firmware information ---</b>' >> log.txt")
    output = runCommand(command)
    runCommand("echo '<b>-----------------------------</b>\n' >> log.txt")
    print(output)
    context = {
        'output': [command] + output.split('\n'),
    }
    return render_template('output.html', **context)


@app.route('/<board>/fpga/<site>/program', methods=['POST'])
def program(board: str, site: str):
    """

    :param site: site of FPGA to run command on.

    """
    package_name = ""
    if request.method == 'POST':
        form = request.form
        try:
            f = request.files['file']
            package_name = f.filename.split(".")[0]
            f.save('uploads/uploaded_package.tgz')
            form = request.form
            runCommand("echo '<b>--- Copying Files ---</b>' >> log.txt")
            runCommand("ssh -p %d cmx@pcuptracker001 rm -rf /home/cmx/tmpfiles/%s" % (
                config["boards"][board]["port"], package_name
            ))
            print(runCommand(
                "scp -P %d uploads/uploaded_package.tgz " % (
                    config["boards"][board]["port"]
                )
                + "cmx@pcuptracker001:/home/cmx/tmpfiles/package.tgz >> log.txt"
            ))
            runCommand(
                "ssh -p %d cmx@pcuptracker001 tar xvf /home/cmx/tmpfiles/package.tgz -C /home/cmx/tmpfiles/ >> log.txt" % (config["boards"][board]["port"])
            )
        except BadRequestKeyError:
            pass
    print(form)
    runCommand("echo '<b>--- Programming FPGA ---</b>' >> log.txt")
    command = "ssh -p %d cmx@pcuptracker001 serenitybutler -c /home/cmx/mp801/serenity.yml program %s /home/cmx/tmpfiles/%s/top.bit >> log.txt" % (config["boards"][board]["port"], site, package_name)
    output = runCommand(command)
    runCommand("echo '<b>-----------------------------</b>\n' >> log.txt")
    context = {
        'output': [command] + output.split('\n'),
    }
    runCommand("ssh -p %d cmx@pcuptracker001 rm -rf /home/cmx/tmpfiles/package.tgz" % (
        config["boards"][board]["port"]
    ))
    return render_template('output.html', **context)


@app.route('/log')
def log():
    with open("log.txt") as logfile:
        lines = [line.strip() for line in logfile]
        print(lines)
        stream = "<br>".join(lines)
        return stream


@app.route('/clear')
def clear():
    try:
        os.remove("log.txt")
    except FileNotFoundError:
        pass
    return "success"
