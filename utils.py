from subprocess import Popen, PIPE, STDOUT


def runCommand(command: str) -> str:
    """Function to clean up the process of running a command within a shell.

    :param command:

    """
    print(command)
    p = Popen(command, shell=True, stdin=PIPE,
              stdout=PIPE, stderr=STDOUT, close_fds=True)
    output: str = p.stdout.read().decode('utf-8').strip()
    return output
